/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID MUSIC_1 = 508837814U;
        static const AkUniqueID MUSIC_LEVEL = 2177735725U;
        static const AkUniqueID SHOTEFX = 4095281152U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace STATE_GAMEGROUP
        {
            static const AkUniqueID GROUP = 1117292370U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STATE_BATTLE = 968190373U;
                static const AkUniqueID STATE_PLAN = 2483705870U;
            } // namespace STATE
        } // namespace STATE_GAMEGROUP

    } // namespace STATES

    namespace SWITCHES
    {
        namespace NEW_SWITCH_GROUP
        {
            static const AkUniqueID GROUP = 2250165792U;

            namespace SWITCH
            {
            } // namespace SWITCH
        } // namespace NEW_SWITCH_GROUP

        namespace SWITCH_INT_GAMEPLAY_INYENCITY
        {
            static const AkUniqueID GROUP = 2736740973U;

            namespace SWITCH
            {
                static const AkUniqueID BATTLE_01 = 1796984003U;
                static const AkUniqueID INTCY_01 = 3921455974U;
                static const AkUniqueID INTCY_02 = 3921455973U;
            } // namespace SWITCH
        } // namespace SWITCH_INT_GAMEPLAY_INYENCITY

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID RTPC_EXT_GAMEPLAYINTENCTY = 1748663663U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID SOUNDBANK = 1661994096U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID NEW_AUDIO_BUS = 2255513057U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
